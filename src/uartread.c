/*----------------------------------------------------------------------------*/
#include "my1uart.h"
#include "my1keys.h"
#include "my1bytes.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
/*----------------------------------------------------------------------------*/
typedef struct _data_t
{
	my1bytes_t data;
	my1uart_t port;
	char* ptty;
	int opts, flag;
	int term, baud;
}
data_t;
/*----------------------------------------------------------------------------*/
#define DATA_DEFAULT_SIZE 64
#define DATA_TIMEOUT_US 100000
/*----------------------------------------------------------------------------*/
void data_init(data_t* data, int argc, char** argv)
{
	int loop, test;
	/* initialize data */
	bytes_init(&data->data);
	bytes_make(&data->data,DATA_DEFAULT_SIZE);
	uart_init(&data->port);
	data->ptty = 0x0;
	data->opts = 0;
	data->flag = 0;
	data->term = 1;
	data->baud = 0;
	/* get command-line options */
	for(loop=1;loop<argc;loop++)
	{
		if(argv[loop][0]=='-') /* options! */
		{
			if(!strcmp(argv[loop],"--port"))
			{
				if(get_param_int(argc,argv,&loop,&test)<0)
					printf("-- Cannot get port number!\n");
				else if(test>MAX_COM_PORT)
					printf("-- Invalid port number? (%d)\n",test);
				else data->term = test;
			}
			else if(!strcmp(argv[loop],"--baud"))
			{
				if(get_param_int(argc,argv,&loop,&test)<0)
					printf("-- Cannot get baudrate!\n");
				else data->baud = test;
			}
			else if(!strcmp(argv[loop],"--tty"))
			{
				if(!(data->ptty=get_param_str(argc,argv,&loop)))
					printf("-- Cannot get device name!\n");
			}
			else printf("-- Unknown option '%s'!\n",argv[loop]);
		}
		else printf("-- Unknown argument '%s'!\n",argv[loop]);
	}
	if(data->ptty) uart_name(&data->port,data->ptty);
	else uart_name(&data->port,"/dev/ttyUSB");
	if(!data->term) data->term = uart_find(&data->port,0);
	if(!uart_prep(&data->port,data->term))
	{
		printf("-- Cannot prepare port '%s'!\n",data->port.temp);
		data->flag |= FLAG_ERROR;
	}
	if(data->baud)
	{
		if (!uart_set_baudrate(&data->port,data->baud))
			printf("-- Cannot set baudrate (%d)!\n",data->baud);
	}
}
/*----------------------------------------------------------------------------*/
void data_free(data_t* data)
{
	uart_done(&data->port);
	bytes_free(&data->data);
}
/*----------------------------------------------------------------------------*/
void data_prep(data_t* data)
{
	/* try opening port */
	if(!uart_open(&data->port))
	{
		printf("-- Cannot open port '%s'!\n",data->port.temp);
		data->flag |= FLAG_ERROR;
		return;
	}
	/* clear input buffer */
	uart_purge(&data->port);
}
/*----------------------------------------------------------------------------*/
void data_show(data_t* data)
{
	int loop;
	printf("\nRX:");
	for (loop=0;loop<data->data.fill;loop++)
		printf("[%02X]",data->data.data[loop]);
	printf("\n");
}
/*----------------------------------------------------------------------------*/
int data_read_timeout(data_t* data, unsigned int delay_us)
{
	struct timeval init, curr, stop;
	int time = 1;
	gettimeofday(&init,0x0);
	curr.tv_sec = delay_us/1000000;
	curr.tv_usec = delay_us%1000000;
	timeradd(&init,&curr,&stop);
	do {
		gettimeofday(&curr,0x0);
		if (uart_incoming(&data->port)) { time = 0; break; }
	} while (timercmp(&curr,&stop,<));
	return time;
}
/*----------------------------------------------------------------------------*/
void data_read(data_t* data)
{
	while (1)
	{
		if (data_read_timeout(data,DATA_TIMEOUT_US))
			break;
		bytes_slip(&data->data,(byte08_t)uart_read_byte(&data->port));
	}
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
	data_t data;
	my1key_t key, esc;
	/* using unbuffered output */
	setbuf(stdout,0);
	do
	{
		/* initialize data struct */
		data_init(&data,argc,argv);
		if (data.flag&FLAG_ERROR) break;
		/* prepare stuff */
		data_prep(&data);
		if (data.flag&FLAG_ERROR) break;
		/* wait for data */
		while (1)
		{
			key = get_keyhit_extended(&esc);
			if (esc==KEY_NONE&&key==KEY_ESCAPE)
			{
				printf("-- User abort!\n");
				break;
			}
			/* check serial port for incoming data */
			if (uart_incoming(&data.port))
			{
				data_read(&data);
				/* show all we got and reset! */
				data_show(&data);
				data.data.fill = 0;
			}
			/* check port status? */
			if (!uart_okstat(&data.port))
			{
				printf("-- Port Error (%d)! Aborting!\n",data.port.stat);
				break;
			}
		}
	}
	while (0);
	/* release data */
	data_free(&data);
	/* done */
	return 0;
}
/*----------------------------------------------------------------------------*/
