# makefile for my1uarttool

TARGET = uarttool
OBJLST = $(TARGET).o my1keys.o my1uart.o my1bytes.o
DOSEND = uartsend
DOREAD = uartread

CC = gcc
DELETE = rm -rf

CFLAGS += -Wall -static $(DOFLAG)
LFLAGS += $(LDFLAGS)
OFLAGS +=
XFLAGS += -D_LARGEFILE_SOURCE=1 -D_FILE_OFFSET_BITS=64

EXTPATH = ../my1codelib/src
CFLAGS += -I$(EXTPATH)

.PHONY: def all new send read clean

# until i have time to actually write $(TARGET) :p
def: $(DOSEND)

all: $(TARGET) $(DOSEND) $(DOREAD)

new: clean def

send: $(DOSEND)

read: $(DOREAD)

$(TARGET): $(OBJLST)
	$(CC) -o $@ $^ $(LFLAGS) $(OFLAGS)

$(DOSEND): uartsend.o my1keys.o my1uart.o my1bytes.o
	$(CC) -o $@ $^ $(LFLAGS) $(OFLAGS)

$(DOREAD): uartread.o my1keys.o my1uart.o my1bytes.o
	$(CC) -o $@ $^ $(LFLAGS) $(OFLAGS)

%.o: src/%.c src/%.h
	$(CC) -c $(CFLAGS) -o $@ $<

%.o: src/%.c
	$(CC) -c $(CFLAGS) -o $@ $<

%.o: $(EXTPATH)/%.c $(EXTPATH)/%.h
	$(CC) -c $(CFLAGS) -o $@ $<

clean:
	-$(DELETE) $(TARGET) $(DOSEND) $(DOREAD) *.o
